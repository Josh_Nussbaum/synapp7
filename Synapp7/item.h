//
//  item.h
//  Synapp7
//
//  Created by Josh on 7/7/14.
//  Copyright (c) 2014 Josh Nussbaum. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Foundation/Foundation.h>

@interface item : NSObject
{
    NSInteger iid;
    NSString *name;
    NSInteger cost;
    NSString *description;
}

//Constructors

-(id) init;

-(id)initWithiid:(NSInteger)iid_ name:(NSString *)name_ cost:(NSInteger)cost_ description:(NSString *)description_;

//Creation Functions

-(void) setIID:(NSInteger)iid_;

-(void) setName:(NSString *)name_;

-(void) setCost:(NSInteger)cost_;

-(void) setDescription:(NSString *)descrption_;

//Read Functions

-(NSInteger) getIID;

-(NSString *) getName;

-(NSInteger) getCost;

-(NSString *) getDescription;

//Update Functions



@end

