//
//  HomeViewController.h
//  Synapp7
//
//  Created by Josh on 6/30/14.
//  Copyright (c) 2014 Josh Nussbaum. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SnowShoeViewController.h"
#import "teacher.h"
#import "category.h"
#import "item.h"
#import "DBManager.h"

@interface StampViewController : SnowShoeViewController<UIAlertViewDelegate, UITextFieldDelegate>
{
    teacher *obj;
}

// Teacher Settings
@property (weak, nonatomic) IBOutlet UIButton *settings;
- (IBAction)logoutClicked:(id)sender;

- (IBAction)backgroundTap:(id)sender;

// Register Student
- (IBAction)registerClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *nextStudent;
- (IBAction)nextStudentClicked:(id)sender;

// Reward Student
- (IBAction)categoriesClicked:(id)sender;
- (IBAction)categoryClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *pointCategory;
@property (weak, nonatomic) IBOutlet UILabel *studentName;
@property (weak, nonatomic) IBOutlet UILabel *studentPoints;

// Items
- (IBAction)itemsClicked:(id)sender;
- (IBAction)itemClicked:(id)sender;

@end
