//
//  item.m
//  Synapp7
//
//  Created by Josh on 7/7/14.
//  Copyright (c) 2014 Josh Nussbaum. All rights reserved.
//

#import "item.h"

@implementation item

//Constructors

-(id) init
{
    self = [super init];
    if (self){
        self->iid = 0;
        self->name = @"";
        self->cost = 0;
        self->description = @"";
    }
    return self;
}

-(id)initWithiid:(NSInteger)iid_ name:(NSString *)name_ cost:(NSInteger)cost_ description:(NSString *)description_
{
    self = [super init];
    if (self) {
        self->iid = iid_;
        self->name = name_;
        self->cost = cost_;
        self->description = description_;
    }
    return self;
}

//Creation Functions

-(void) setIID:(NSInteger)iid_
{
    self->iid = iid_;
}

-(void) setName:(NSString *)name_
{
    self->name = name_;
}

-(void) setCost:(NSInteger)cost_
{
    self->cost = cost_;
}

-(void) setDescription:(NSString *)description_
{
    self->description = description_;
}

//Read Functions

-(NSInteger) getIID
{
    return self->iid;
}

-(NSString *) getName
{
    return self->name;
}

-(NSInteger) getCost
{
    return self->cost;
}

-(NSString *) getDescription
{
    return self->description;
}

@end
