//
//  DBManager.m
//  Synapp7
//
//  Created by Josh on 7/2/14.
//  Copyright (c) 2014 Josh Nussbaum. All rights reserved.
//

#import "DBManager.h"
static DBManager *sharedInstance = nil;
static sqlite3 *database = nil;
static sqlite3_stmt *statement = nil;

#define TimeStamp [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000]

@implementation DBManager

+(DBManager*)getSharedInstance{
    if (!sharedInstance) {
        sharedInstance = [[super allocWithZone:NULL]init];
        [sharedInstance createDB];
    }
    return sharedInstance;
}

-(BOOL)createDB{
    NSString *docsDir;
    NSArray *dirPaths;
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString:
                    [docsDir stringByAppendingPathComponent: @"SynappDB.db"]];
    BOOL isSuccess = YES;
    
    //Create filemanager and use it to check if database file already exists
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    /*if([[NSFileManager defaultManager] fileExistsAtPath:databasePath]){
        [[NSFileManager defaultManager] removeItemAtPath:databasePath error:nil];
    }*/
    if ([filemgr fileExistsAtPath: databasePath ] == YES)
    {
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            //Create Tables
            char *errMsg;
            const char *create_tables = "DROP TABLE IF EXISTS Items;"
                                        "DROP TABLE IF EXISTS UnregStudents;"
                                        "DROP TABLE IF EXISTS RegStudents;"
                                        "DROP TABLE IF EXISTS Categories;"
                                        "DROP TABLE IF EXISTS Timestamps;"
                                        "DROP TABLE IF EXISTS Points;"
                                        "DROP TABLE IF EXISTS Transactions;"
                                        "CREATE TABLE UnregStudents (uid integer primary key, name text, lvl integer, score integer, progress integer, lvlupamount integer, isactivated integer, serial text);"
                                        "CREATE TABLE RegStudents (uid integer primary key, name text, lvl integer, score integer, pointsearned integer, pointsspent integer, progress integer, lvlupamount integer, isactivated integer, serial text, timestamp text);"
                                        "CREATE TABLE Items (iid integer primary key, name text, cost integer, description text);"
                                        "CREATE TABLE Categories (cid integer primary key, name text);"
                                        "CREATE TABLE Timestamps (uid integer, timestamp text);"
                                        "CREATE TABLE Points (uid integer, cid integer, timestamp text);"
                                        "CREATE TABLE Transactions(uid integer, iid integer, timestamp text);";

            
            if (sqlite3_exec(database, create_tables, NULL, NULL, &errMsg) == SQLITE_OK)
            {
                sqlite3_close(database);
                NSLog(@"Successfully created the database and tables");
                return  isSuccess;
            }
            else
            {
                isSuccess = NO;
                NSLog(@"Failed to create table");
            }

        }
        else {
            isSuccess = NO;
            NSLog(@"Failed to open/create database");
        }
    }
    return isSuccess;
}

- (NSString *) timeStamp {
    return [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
}

/* ============= SETTER FUNCTIONS =============
   =============                  ============= */


-(void) setStudents:(NSMutableArray *)students
{
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        for (NSUInteger i = 0, count = [students count]; i < count; i++)
        {
            NSDictionary *student = [students objectAtIndex:i];
            NSInteger isActivated = [student[@"isActivated"] intValue];
            NSInteger currentScore = [student[@"currentScore"] intValue];
            NSInteger lvl = [student[@"lvl"] intValue];
            NSInteger lvlUpAmount = [student[@"lvlUpAmount"] intValue];
            NSInteger progress = [student[@"progress"] intValue];
            NSInteger uid = [student[@"uid"] intValue];
            NSString *name = student[@"name"];
            NSString *serial = student[@"stamp"];
            NSString *timestamp = student[@"lastUpdate"];
            //"CREATE TABLE RegStudents (uid integer primary key, name text, lvl integer, score integer, progress integer, lvlupamount integer, isactivated integer, serial text, timestamp text);"
            NSString *querySQL = [NSString stringWithFormat:
                                  @"Insert INTO RegStudents (uid, name, lvl, score, pointsearned, pointsspent, progress, lvlupamount, isactivated, serial, timestamp) VALUES (%d,\"%@\",%d,%d, 0, 0, %d,%d,%d, \"%@\", \"%@\")", uid, name, lvl, currentScore, progress, lvlUpAmount, isActivated, serial, timestamp ];
            
            const char *insert_stmt = [querySQL UTF8String];
            sqlite3_prepare_v2(database, insert_stmt,
                               -1, &statement, NULL);
            sqlite3_step(statement);
            sqlite3_finalize(statement);

            
            NSString *querySQL2 = [NSString stringWithFormat:
                                  @"Insert INTO Timestamps (uid, timestamp) VALUES (%d,\"%@\")", uid, timestamp ];
            const char *insert_stmt2 = [querySQL2 UTF8String];
            sqlite3_prepare_v2(database, insert_stmt2,
                               -1, &statement, NULL);
            sqlite3_step(statement);
            sqlite3_finalize(statement);
        }
    }
    sqlite3_close(database);
}

-(void) setUnregisteredStudents:(NSMutableArray *)students
{
    sqlite3_stmt *statement;
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        for (NSUInteger i = 0, count = [students count]; i < count; i++)
        {
            NSDictionary *student = [students objectAtIndex:i];
            NSInteger isActivated = [student[@"isActivated"] intValue];
            NSInteger currentScore = [student[@"currentScore"] intValue];
            NSInteger lvl = [student[@"lvl"] intValue];
            NSInteger lvlUpAmount = [student[@"lvlUpAmount"] intValue];
            NSInteger progress = [student[@"progress"] intValue];
            NSInteger uid = [student[@"uid"] intValue];
            NSString *name = student[@"name"];
            NSString *serial = @"";
            NSString *querySQL = [NSString stringWithFormat:
                                  @"Insert INTO UnregStudents (uid, name, lvl, score, progress, lvlupamount, isactivated, serial) VALUES (%d,\"%@\",%d,%d,%d,%d,%d, \"%@\")", uid, name, lvl, currentScore, progress, lvlUpAmount, isActivated, serial ];
            const char *query_stmt = [querySQL UTF8String];
            sqlite3_prepare_v2(database, query_stmt,-1, &statement, NULL);
            sqlite3_step(statement);
            sqlite3_finalize(statement);
        }
    }
    sqlite3_close(database);

}


-(void) setItems:(NSMutableArray *)items
{
    sqlite3_stmt *statement;
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        for (NSUInteger i = 0, count = [items count]; i < count; i++)
        {
            NSDictionary *item = [items objectAtIndex:i];
            NSInteger iid = [item[@"iid"] intValue];
            NSString *name = item[@"name"];
            NSInteger cost = [item[@"cost"]intValue];
            NSString *description = item[@"descr"];
            //NSString *description = item[@"descr"];
            NSString *querySQL = [NSString stringWithFormat:
                                  @"Insert INTO Items (iid, name, cost, description) VALUES (%d,\"%@\", %d, \"%@\" )", iid, name, cost, description];
            const char *query_stmt = [querySQL UTF8String];
            sqlite3_prepare_v2(database, query_stmt,-1, &statement, NULL);
            sqlite3_step(statement);
            sqlite3_finalize(statement);
        }
    }
    sqlite3_close(database);
}


-(void) setCategories:(NSMutableArray *)categories
{
    sqlite3_stmt *statement;
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        for (NSUInteger i = 0, count = [categories count]; i < count; i++)
        {
            NSDictionary *category = [categories objectAtIndex:i];
            NSInteger cid = [category[@"categoryId"] intValue];
            NSString *name = category[@"name"];
            //NSString *description = item[@"description"];
            //uid integer primary key, name text, score integer, progress integer, lvlupamount integer, isactivated integer
            NSString *querySQL = [NSString stringWithFormat:
                                  @"Insert INTO Categories (cid, name) VALUES (%d,\"%@\")", cid, name];
            const char *query_stmt = [querySQL UTF8String];
            sqlite3_prepare_v2(database, query_stmt,-1, &statement, NULL);
            sqlite3_step(statement);
            sqlite3_finalize(statement);
        }
    }
    sqlite3_close(database);
}



/* ============= ACCESS FUNCTIONS =============
   =============                  ============= */

-(NSMutableArray *) getUnregisteredStudents
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:
                              @"SELECT * from UnRegStudents"];
        const char *query_stmt = [querySQL UTF8String];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        if (sqlite3_prepare_v2(database,
                               query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                //        "CREATE TABLE RegStudents (uid integer primary key, name text, lvl integer, score integer, pointsearned integer, progress integer, lvlupamount integer, isactivated integer, serial text, timestamp text);"
                
                NSInteger uid = sqlite3_column_int(statement, 0);
                
                NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
                
                NSInteger lvl = sqlite3_column_int(statement, 2);
                
                NSInteger score = sqlite3_column_int(statement, 3);
                
                NSInteger progress = sqlite3_column_int(statement, 4);
                
                NSInteger lvlupamount = sqlite3_column_int(statement, 5);
                
                NSInteger isactivated = sqlite3_column_int(statement, 6);

                
                student *ss = [[student alloc] initWithuid:uid name:name lvl:lvl lvlupamount:lvlupamount progress:progress serial:@"" score:score pointsearned:0 pointsspent:0 isactivated:isactivated timestamp:@""];

                [resultArray addObject:ss];

            } 
            return resultArray;
            sqlite3_reset(statement);
        }
    }
    return nil;
}

-(NSMutableArray *) getStudents
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:
                              @"SELECT * from RegStudents"];
        const char *query_stmt = [querySQL UTF8String];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        if (sqlite3_prepare_v2(database,
                               query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                
                //                     "CREATE TABLE RegStudents (uid integer primary key, name text, lvl integer, score integer, progress integer, lvlupamount integer, isactivated integer, serial text, timestamp text);
                
                NSInteger uid = sqlite3_column_int(statement, 0);
                
                NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
                
                NSInteger lvl = sqlite3_column_int(statement, 2);
                
                NSInteger score = sqlite3_column_int(statement, 3);
                
                NSInteger pointsearned = sqlite3_column_int(statement, 4);
                
                NSInteger pointsspent = sqlite3_column_int(statement, 5);
                
                NSInteger progress = sqlite3_column_int(statement, 6);
                
                NSInteger lvlupamount = sqlite3_column_int(statement, 7);
                
                NSInteger isactivated = sqlite3_column_int(statement, 8);
                
                NSString *serial = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 9)];
                
                NSString *timestamp = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 10)];

                
                
                student *ss = [[student alloc] initWithuid:uid name:name lvl:lvl lvlupamount:lvlupamount progress:progress serial:serial score:score pointsearned:pointsearned pointsspent:pointsspent isactivated:isactivated timestamp:timestamp ];
                
                [resultArray addObject:ss];
                
            }
            return resultArray;
            sqlite3_reset(statement);
        }
    }
    return nil;
}

-(student *)getStudent:(NSString *)serial
{
    NSMutableArray *students = [[DBManager getSharedInstance] getStudents];
    NSInteger keyCount = [students count];
    NSInteger index = 0;
    while (index != keyCount)
    {
        student *ss = [students objectAtIndex:index++];
        NSString *regSerial = [ss getSerial];
        if ([serial isEqualToString:regSerial]) return ss;
    }
    return nil;
}



-(NSMutableArray *) getCategories
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:
                              @"SELECT * from Categories"];
        const char *query_stmt = [querySQL UTF8String];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        if (sqlite3_prepare_v2(database,
                               query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                
                NSInteger cid = sqlite3_column_int(statement, 0);

                NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];

                category *cc = [[category alloc] initWithcid:cid name:name];
                
                [resultArray addObject:cc];
                
            }
            return resultArray;
            sqlite3_reset(statement);
        }
    }
    return nil;
}

-(NSMutableArray *) getItems
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:
                              @"SELECT * from Items"];
        const char *query_stmt = [querySQL UTF8String];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        if (sqlite3_prepare_v2(database,
                               query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                // "CREATE TABLE IF NOT EXISTS Items (iid integer primary key, name text, cost integer, description text);"

                
                NSInteger iid = sqlite3_column_int(statement, 0);
                
                NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];

                NSInteger cost = sqlite3_column_int(statement, 2);

                NSString *description = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];

                
                item *ii = [[item alloc] initWithiid:iid name:name cost:cost description:description];
                
                [resultArray addObject:ii];
                            
            }
            return resultArray;
            sqlite3_reset(statement);
        }
    }
    return nil;
}

-(item *)           getItem:(NSInteger )iid
{
    NSMutableArray *items = [[DBManager getSharedInstance] getItems];
    NSInteger keyCount = [items count];
    NSInteger index = 0;
    while (index != keyCount)
    {
        item *ii = [items objectAtIndex:index++];
        NSInteger id = [ii getIID];
        if (iid == id) return ii;
    }
    return nil;
}

/* ============= UPDATE FUNCTIONS =============
 =============                   ============= */


-(void) addStudent:(student *)ss
{
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        [ss setTimestamp:[self getDate]];
        NSString *querySQL = [NSString stringWithFormat:
                              @"Insert INTO RegStudents (uid, name, lvl, score, progress, lvlupamount, isactivated, serial, timestamp) VALUES (%d,\"%@\",%d,%d,%d,%d,%d, \"%@\", \"%@\")", [ss getUID], [ss getName], [ss getLvl], [ss getScore], [ss getProgress], [ss getLvlupamount], [ss getIsactivated], [ss getSerial], [ss getTimestamp] ];
        const char *insert_stmt = [querySQL UTF8String];
        sqlite3_prepare_v2(database, insert_stmt,
                           -1, &statement, NULL);
        sqlite3_step(statement);
        sqlite3_finalize(statement);
    }
    sqlite3_close(database);

}

-(void) removeUnregisteredStudent:(student *)ss
{
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:
                               @"DELETE FROM UnregStudents WHERE uid=%d", [ss getUID] ];
        const char *delete_stmt = [querySQL UTF8String];
        sqlite3_prepare_v2(database, delete_stmt,
                           -1, &statement, NULL);
        sqlite3_step(statement);
        sqlite3_finalize(statement);
    }
    sqlite3_close(database);
}


/* =============  MISC FUNCTIONS =============
 =============                   ============= */

-(NSString *) getDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"GMT"]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateAsString = [formatter stringFromDate:[NSDate date]];
    return dateAsString;
}

-(BOOL) isStampRegistered:(NSString *)serial
{
    NSMutableArray *students = [[DBManager getSharedInstance] getStudents];
    NSInteger keyCount = [students count];
    NSInteger index = 0;
    while (index != keyCount)
    {
        student *ss = [students objectAtIndex:index++];
        NSString *regSerial = [ss getSerial];
        if ([serial isEqualToString:regSerial]) return true;
    }
    return false;
}

-(void) addPoint:(student *)ss
{
    obj = [teacher getInstance];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *ts = [self getDate];
        [ss setTimestamp:ts];
        
        [ss addPoint];
        NSString *querySQL = [NSString stringWithFormat:
                              @"UPDATE RegStudents SET score=%d, pointsearned=%d, timestamp =\"%@\" WHERE uid=%d", [ss getScore], [ss getPointsearned], ts, [ss getUID]];
        const char *update_stmt = [querySQL UTF8String];
        sqlite3_prepare_v2(database, update_stmt,
                           -1, &statement, NULL);
        int rc;
        rc = sqlite3_step(statement);
        if (rc != SQLITE_DONE)
            NSLog(@"%s: step error: %d: %s", __FUNCTION__, rc, sqlite3_errmsg(statement));
        sqlite3_finalize(statement);
        
        
        NSString *querySQL3 = [NSString stringWithFormat:
                               @"INSERT INTO Points (cid, uid, timestamp) VALUES (%d, %d, \"%@\")", obj.categoryId, [ss getUID], ts];
        const char *insert_stmt2 = [querySQL3 UTF8String];
        NSLog(@"Add point QUERY => %@", querySQL3);
        sqlite3_prepare_v2(database, insert_stmt2,
                           -1, &statement, NULL);
        rc = sqlite3_step(statement);
        if (rc != SQLITE_DONE)
            NSLog(@"%s: step error: %d: %s", __FUNCTION__, rc, sqlite3_errmsg(statement));
        sqlite3_finalize(statement);
        
    }
    sqlite3_close(database);
}

-(void) sellItemWithitem:(item * )ii student:(student *)ss score:(NSInteger)score_ pointsEarned:(NSInteger)pointsEarned_;
{
    // If Student can afford the item return his points after the transaction. Else Return -1.
    // Update the RegStudents and Points database .
    NSInteger uid = [ss getUID];
    NSInteger iid = [ii getIID];
    [ss setScore:score_];
    [ss setPointsspent:[ii getCost]];
    NSLog(@"Points Earned => %d", pointsEarned_);
    [ss setPointsearned:pointsEarned_];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *ts = [self getDate];
        NSString *querySQL = [NSString stringWithFormat:
                              @"UPDATE RegStudents SET score=%d, pointsearned=%d, timestamp =\"%@\" WHERE uid=%d",score_, pointsEarned_, ts ,uid];
        //NSLog(@"Query = > %@", querySQL);
        const char *update_stmt = [querySQL UTF8String];
        sqlite3_prepare_v2(database, update_stmt,
                           -1, &statement, NULL);
        sqlite3_step(statement);
        sqlite3_finalize(statement);
        
        NSString *querySQL3 = [NSString stringWithFormat:
                               @"INSERT INTO Transactions (uid, iid, timestamp) VALUES (%d, %d, \"%@\")", uid, iid, ts];
        //NSLog(@"Query3 = > %@", querySQL3);
        const char *insert_stmt2 = [querySQL3 UTF8String];
        sqlite3_prepare_v2(database, insert_stmt2,
                           -1, &statement, NULL);
        sqlite3_step(statement);
        sqlite3_finalize(statement);
        
    }
    sqlite3_close(database);
}

-(void) pushChanges
{
    NSLog(@"Push Changes");
    const char *dbpath = [databasePath UTF8String];
    NSMutableString *jsonData;
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        
        // Update Students SCORE and ISACTIVATED
        NSLog(@"We right before the query");
        NSString *querySQL = [NSString stringWithFormat:
                              @"SELECT * FROM RegStudents WHERE timestamp NOT IN (SELECT timestamp FROM Timestamps)"];
        const char *query_stmt = [querySQL UTF8String];
        jsonData = [[NSMutableString alloc]initWithString:@""];
        if (sqlite3_prepare_v2(database,
                               query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if(sqlite3_column_text(statement, 0) != nil)
            {
                
            }
            [jsonData appendString:@"\"Updates\":{\"Students\":{("];
            
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                
                NSInteger uid = sqlite3_column_int(statement, 0);
                
                NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
                
                NSInteger lvl = sqlite3_column_int(statement, 2);
                
                NSInteger score = sqlite3_column_int(statement, 3);
                
                NSInteger pointsearned = sqlite3_column_int(statement, 4);
                
                NSInteger pointsspent = sqlite3_column_int(statement, 5);
                
                NSInteger progress = sqlite3_column_int(statement, 6);
                
                NSInteger lvlupamount = sqlite3_column_int(statement, 7);
                
                NSInteger isactivated = sqlite3_column_int(statement, 8);
                
                NSString *serial = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 9)];
                
                NSString *timestamp = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 10)];
                
                [jsonData appendFormat:@"(\"uid\":%d, \"lvl\":%d, \"pointsspent\":%d, \"pointsearned\":%d, \"progress\":%d, \"lvlupamount\":%d, \"isactivated\":%d, \"timestamp\":\"%@\")", uid, lvl, pointsspent, pointsearned, progress, lvlupamount, isactivated,  timestamp];
                
            }
            
            
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                
                NSInteger uid = sqlite3_column_int(statement, 0);
                NSLog(@"UID => %d", uid);
                
                NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
                
                NSInteger lvl = sqlite3_column_int(statement, 2);
                
                NSInteger score = sqlite3_column_int(statement, 3);
                
                NSInteger pointsearned = sqlite3_column_int(statement, 4);
                
                NSInteger pointsspent = sqlite3_column_int(statement, 5);
                
                NSInteger progress = sqlite3_column_int(statement, 6);
                
                NSInteger lvlupamount = sqlite3_column_int(statement, 7);
                
                NSInteger isactivated = sqlite3_column_int(statement, 8);
                
                NSString *serial = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 9)];
                
                NSString *timestamp = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 10)];
                NSLog(@"timestamp => %@", timestamp);

                [jsonData appendFormat:@",(\"uid\":%d, \"lvl\":%d, \"pointsspent\":%d, \"pointsearned\":%d, \"progress\":%d, \"lvlupamount\":%d, \"isactivated\":%d, \"timestamp\":\"%@\")", uid, lvl, pointsspent, pointsearned, progress, lvlupamount, isactivated,  timestamp];
                
            }
            sqlite3_reset(statement);
            
        }
        
        // Update the Points Table
        NSString *querySQL2 = [NSString stringWithFormat:
                               @"SELECT * from Points"];
        const char *query_stmt2 = [querySQL2 UTF8String];
        if (sqlite3_prepare_v2(database,
                               query_stmt2, -1, &statement, NULL) == SQLITE_OK)
        {
            [jsonData appendString:@")}\"Points\":{("];
            
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                NSInteger cid = sqlite3_column_int(statement, 0);
                
                NSInteger uid = sqlite3_column_int(statement, 1);
                
                NSString *timestamp = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
                
                [jsonData appendFormat:@"(\"uid\":%d, \"cid\":%d, \"timestamp\":\"%@\")", uid, cid, timestamp];
            }

   
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                
                
                NSInteger cid = sqlite3_column_int(statement, 0);
                
                NSInteger uid = sqlite3_column_int(statement, 1);
                
                NSString *timestamp = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
                
                [jsonData appendFormat:@", (\"uid\":%d, \"cid\":%d, \"timestamp\":\"%@\")", uid, cid, timestamp];
                
                
            }
            sqlite3_reset(statement);
            
            // Update the Transactions Table
            NSString *querySQL3 = [NSString stringWithFormat:
                                   @"SELECT * from Transactions"];
            const char *query_stmt3 = [querySQL3 UTF8String];
            if (sqlite3_prepare_v2(database,
                                   query_stmt3, -1, &statement, NULL) == SQLITE_OK)
            {
                [jsonData appendString:@")}\"Transactions\":{("];
                
                if (sqlite3_step(statement) == SQLITE_ROW)
                {
                    
                    
                    NSInteger uid = sqlite3_column_int(statement, 0);
                    
                    NSInteger iid = sqlite3_column_int(statement, 1);
                    
                    NSString *timestamp = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
                    
                    [jsonData appendFormat:@"(\"uid\":%d, \"iid\":%d, \"timestamp\":\"%@\")", uid, iid, timestamp];
                    
                }
                
                while (sqlite3_step(statement) == SQLITE_ROW)
                {
                    
                    
                    NSInteger uid = sqlite3_column_int(statement, 0);
                    
                    NSInteger iid = sqlite3_column_int(statement, 1);
                    
                    NSString *timestamp = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
                    
                    [jsonData appendFormat:@",(\"uid\":%d, \"iid\":%d, \"timestamp\":\"%@\")", uid, iid, timestamp];
                    
                }
                sqlite3_reset(statement);
                [jsonData appendString:@")}}"];
                NSLog(@"JSON DATA TO SEND OFF => %@", jsonData);

                
            }
        }
        // Now Push the jsonData string to the web service
        NSURL *url = [NSURL URLWithString:@"http://162.224.188.176:8009/SynappWebServiceDemo/services/update/students"];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                           timeoutInterval:10];
        NSData *requestData = [NSData dataWithBytes:[jsonData UTF8String] length:[jsonData length]];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: requestData];
        
        NSError *error = [[NSError alloc] init];
        NSHTTPURLResponse *response = nil;
        NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    }
}

-(void) updateTables
{
    //Now GET Request
    
    NSURL *url = [NSURL URLWithString:@"http://162.224.188.176:8009/SynappWebServiceDemo/services/update/students"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:10];
    [request setHTTPMethod:@"GET"];
    
    NSError *error = [[NSError alloc] init];
    NSHTTPURLResponse *response = nil;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSError *err = nil;
    NSDictionary *jsonData = [NSJSONSerialization
                              JSONObjectWithData:urlData
                              options:NSJSONReadingMutableContainers
                              error:&err];
    NSLog(@"Web Server DATA => %@", jsonData);
    NSMutableArray *unregStudents = jsonData[@"unregStudents"];
    NSMutableArray *regStudents = jsonData[@"regStudents"];
    NSMutableArray *items = jsonData[@"Items"];
    NSMutableArray *categories = jsonData[@"Categories"];

    // Set teacher properties
    obj.tid = [[[jsonData objectForKey:@"login"] objectForKey:@"uid"] intValue];
    obj.cid = [[[jsonData objectForKey:@"login"] objectForKey:@"cid"] intValue];
    
    // Update database tables.
    [[DBManager getSharedInstance]setStudents:regStudents];
    [[DBManager getSharedInstance]setUnregisteredStudents:unregStudents];
    [[DBManager getSharedInstance]setItems:items];
    [[DBManager getSharedInstance]setCategories:categories];
}


@end
