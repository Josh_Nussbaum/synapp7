//
//  student.m
//  Synapp7
//
//  Created by Josh on 7/3/14.
//  Copyright (c) 2014 Josh Nussbaum. All rights reserved.
//

#import "student.h"

static student *sharedstudent = nil;

@implementation student

// Creation Functions
+(instancetype)createStudent
{
    if (!sharedstudent) {
        sharedstudent = [[super allocWithZone:NULL]init];
    }
    return sharedstudent;
}

// Constructors
-(id) init
{
    self = [super init];
    if (self){
        self->uid = 0;
        self->name = @"";
        self->lvl = 0;
        self->lvlupamount = 0;
        self->progress = 0;
        self->serial = @"";
        self->score = 0;
        self->isactivated = 0;
        self->timestamp = @"";
        self->pointsearned = 0;
        self->pointsspent = 0;
    }
    return self;
}

-(id) initWithuid:(NSInteger)uid_ name:(NSString *)name_ lvl:(NSInteger)lvl_ lvlupamount:(NSInteger)lvlupamount_ progress:(NSInteger)progress_ serial:(NSString *)serial_ score:(NSInteger)score_ pointsearned:(NSInteger)pointsearned_ pointsspent:(NSInteger)pointsspent_ isactivated:(NSInteger)isactivated_ timestamp:(NSString *)timestamp_
{
    self = [super init];
    if (self) {
        self->uid = uid_;
        self->name = name_;
        self->lvl = lvl_;
        self->lvlupamount = lvlupamount_;
        self->progress = progress_;
        self->serial = serial_;
        self->score = score_;
        self->isactivated = isactivated_;
        self->timestamp = timestamp_;
        self->pointsearned = pointsearned_;
        self->pointsspent = pointsspent_;
    }
    return self;
}


// Creation Functions
- (void)setUID:(NSInteger )uid_
{
    self->uid = uid_;
}

- (void)setName:(NSString *)name_
{
    self->name = name_;
}

- (void)setLvl:(NSInteger )lvl_
{
    self->lvl = lvl_;
}

- (void)setScore:(NSInteger )score_
{
    self->score = score_;
}

- (void)setProgress:(NSInteger )progress_
{
    self->progress = progress_;
}

- (void)setLvlupamount:(NSInteger )lvlupamount_
{
    self->lvlupamount = lvlupamount_;
}

- (void)setIsactivated:(NSInteger )isactivated_
{
    self->isactivated = isactivated_;
}

- (void)setSerial:(NSString *)serial_
{
    self->serial = serial_;
}

- (void)setTimestamp:(NSString *)timestamp_
{
    self->timestamp = timestamp_;
}

-(void)setPointsearned:(NSInteger )pointsearned_
{
    self->pointsearned = pointsearned_;
}

-(void)setPointsspent:(NSInteger)pointsspent_
{
    self->pointsspent = (self->pointsspent + pointsspent_);
}

// Read Functions

- (NSInteger )getUID
{
    return self->uid;
}
- (NSString *)getName
{
    return self->name;
}
- (NSInteger )getLvl
{
    return self->lvl;
}
- (NSInteger )getScore
{
    return self->score;
}
- (NSInteger )getProgress
{
    return self->progress;
}
- (NSInteger )getLvlupamount
{
    return self->lvlupamount;
}
- (NSInteger )getIsactivated
{
    return self->isactivated;
}
- (NSString *)getSerial
{
    return self->serial;
}
- (NSString *)getTimestamp
{
    return self->timestamp;
}
- (NSInteger )getPointsearned
{
    return self->pointsearned;
}
- (NSInteger )getPointsspent
{
    return self->pointsspent;
}

// Update Functions

-(void) addPoint
{
    self->score++;
    self->pointsearned++;
}



@end
