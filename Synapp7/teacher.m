//
//  teacher.m
//  Synapp5
//
//  Created by Josh on 6/30/14.
//  Copyright (c) 2014 Josh Nussbaum. All rights reserved.
//

#import "teacher.h"

@implementation teacher
@synthesize tid;
@synthesize cid;
@synthesize categoryId;
@synthesize itemId;
@synthesize stampType;
@synthesize registerIndex;
@synthesize unregisteredStudentsCount;
@synthesize categoriesCount;
@synthesize itemCount;

static teacher *instance = nil;

+(teacher *)getInstance
{
    @synchronized(self)
    {
        if(instance==nil)
        {
            instance= [teacher new];
            instance.stampType = @"Reward";
            instance.categoryId=0;
        }
    }
    return instance;
}



@end