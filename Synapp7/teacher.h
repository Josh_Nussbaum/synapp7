//
//  teacher.h
//  Synapp5
//
//  Created by Josh on 6/30/14.
//  Copyright (c) 2014 Josh Nussbaum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface teacher : NSObject
{
    NSInteger tid;
    NSInteger cid;
    NSInteger unregisteredStudentsCount;
    NSInteger categoriesCount;
    NSInteger itemCount;
    NSString *stampType;
    NSInteger categoryId;
    NSInteger itemId;
    NSInteger registerIndex;
}

@property(nonatomic)NSInteger tid;
@property(nonatomic)NSInteger cid;
@property(nonatomic)NSInteger unregisteredStudentsCount;
@property(nonatomic, strong)NSString *stampType;
@property(nonatomic)NSInteger categoryId;
@property(nonatomic)NSInteger itemId;
@property(nonatomic)NSInteger registerIndex;
@property(nonatomic)NSInteger categoriesCount;
@property(nonatomic)NSInteger itemCount;

+(teacher*)getInstance;

@end