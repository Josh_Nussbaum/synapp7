//
//  ViewController.m
//  Synapp5
//
//  Created by Josh on 6/30/14.
//  Copyright (c) 2014 Josh Nussbaum. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [DBManager getSharedInstance];
    obj = [teacher getInstance];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)backgroundTap:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)exit:(id)sender
{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Exit"
                                                      message:@"Are you sure?"
                                                     delegate:self
                                            cancelButtonTitle:@"Back"
                                            otherButtonTitles:nil];
    [message addButtonWithTitle:@"Yes"];
    [message setTag:1];
    [message show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1){
        if (buttonIndex > 0) {
            exit(0);
        }
    }
}

- (IBAction)loginValidate:(id)sender {
    NSString *success;
    NSString *email = _emailTextField.text;
    NSString *password = _passwordTextField.text;
    @try {
        if([email isEqualToString:@""] || [password isEqualToString:@""])
        {
            UIAlertView *errAlert = [[UIAlertView alloc] initWithTitle:@"Missing Fields" message:@"Email/Password Required" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
            [errAlert show];
        }
        else
        {
            //Now POST Request
            NSString *jsonRequest = [[NSString alloc] initWithFormat:@"{\"credentials\":{\"username\":\"%@\",\"password\":\"%@\"}}", email, password];
            
            NSURL *url = [NSURL URLWithString:@"http://162.224.188.176:8009/SynappWebServiceDemo/services/login/auth"];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                                   cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                               timeoutInterval:10];
            NSData *requestData = [NSData dataWithBytes:[jsonRequest UTF8String] length:[jsonRequest length]];
            
            [request setHTTPMethod:@"POST"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
            [request setHTTPBody: requestData];
            
            NSError *error = [[NSError alloc] init];
            NSHTTPURLResponse *response = nil;
            NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if([response statusCode] >= 200 && [response statusCode] < 300)
            {
                
                
                NSError *err = nil;
                NSDictionary *jsonData = [NSJSONSerialization
                                          JSONObjectWithData:urlData
                                          options:NSJSONReadingMutableContainers
                                          error:&err];
                NSLog(@"Web Server DATA => %@", jsonData);
                NSMutableArray *unregStudents = jsonData[@"unregStudents"];
                NSMutableArray *regStudents = jsonData[@"regStudents"];
                NSMutableArray *items = jsonData[@"Items"];
                NSMutableArray *categories = jsonData[@"Categories"];
                success = [[jsonData objectForKey:@"login"] objectForKey:@"success"];
                
                NSLog(@"Login Success ==> %@", success);
                if ([success isEqualToString: @"true"])
                {
                    // Set teacher properties
                    obj.tid = [[[jsonData objectForKey:@"login"] objectForKey:@"uid"] intValue];
                    obj.cid = [[[jsonData objectForKey:@"login"] objectForKey:@"cid"] intValue];
                    
                    // Update database tables.
                    [[DBManager getSharedInstance]setStudents:regStudents];
                    [[DBManager getSharedInstance]setUnregisteredStudents:unregStudents];
                    [[DBManager getSharedInstance]setItems:items];
                    [[DBManager getSharedInstance]setCategories:categories];
                }
                else
                {
                    NSLog(@"Login Unsuccessful. Response Code ==> @%ld", (long)[response statusCode]);
                    NSString *error_msg = @"Invalid email or password";
                    [self alertStatus:error_msg :@"Sign in Failed!"];
                }
            }
            else
            {
                [self alertStatus:@"Connection Failed" :@"Sign in Failed!"];
            }
            
        }
    }
    @catch (NSException *e)
    {
        NSLog(@"Exception: %@", e);
        [self alertStatus:@"Error with Sign In." :@"Check Log!"];
    }
    if ([success isEqualToString: @"true"])
    {
        [self performSegueWithIdentifier:@"login_success" sender:self];
    }
}

- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView =[[UIAlertView alloc] initWithTitle:title
                                                       message:msg
                                                      delegate:self
                                             cancelButtonTitle:@"ok"
                                             otherButtonTitles:nil,nil];
    [alertView show];
    
}

@end
