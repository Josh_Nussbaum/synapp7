//
//  category.h
//  Synapp7
//
//  Created by Josh on 7/7/14.
//  Copyright (c) 2014 Josh Nussbaum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface category : NSObject
{
    NSInteger cid;
    NSString *name;
}

-(id) init;

-(id) initWithcid:(NSInteger)cid_ name:(NSString *)name_;

// Creation Functions

-(void) setCID:(NSInteger)cid;

-(void) setName:(NSString *)name;

// Read Functions

-(NSInteger) getCID;

-(NSString *) getName;

// Update Functions

@end
