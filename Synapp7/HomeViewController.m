//
//  HomeViewController.m
//  Synapp7
//
//  Created by Josh on 6/30/14.
//  Copyright (c) 2014 Josh Nussbaum. All rights reserved.
//

#import "HomeViewController.h"

static NSString * const snowshoe_app_key = @"77a68361b235de06fc14";
static NSString * const snowshoe_app_secret = @"7ccfc2d407733d94fdd7d992b8b0080259d23d5d";

@interface StampViewController ()

@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation StampViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //Set up SnowShoe
    
    [DBManager getSharedInstance];
    self.appKey = snowshoe_app_key ;
    self.appSecret = snowshoe_app_secret;
    [self.nextStudent setTitle:@"" forState:UIControlStateNormal];
    [self.nextStudent setEnabled:NO];
    [self.pointCategory setTitle:@"" forState:UIControlStateNormal];
    [self.pointCategory setEnabled:NO];
    [self.studentPoints setText:@""];
    [self.pointCategory setEnabled: NO];
    [self.settings setEnabled:YES];
    [self.settings setTitle:@"logout" forState:UIControlStateNormal];

    obj = [teacher getInstance];
    obj.stampType = @"Reward";

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)logoutClicked:(id)sender
{
    UIAlertView *alertView =[[UIAlertView alloc] initWithTitle:@"Logout"
                                                       message:@"Are you sure?"
                                                      delegate:self
                                             cancelButtonTitle:@"Back"
                                             otherButtonTitles:nil,nil];
    [alertView addButtonWithTitle:@"Yes"];
    [alertView setTag:6];
    [alertView show];
}


- (IBAction)backgroundTap:(id)sender
{
    [self.view endEditing:YES];
}

-(void)stampResultDidChange:(NSString *)stampResult
{
    if ([stampResult isEqualToString:waiting]) {
        [self.activityIndicator startAnimating];
        return;
    } else {
        [self.activityIndicator stopAnimating];
    }
    NSData *jsonData = [stampResult dataUsingEncoding:NSUTF8StringEncoding];
    NSError *e;
    NSDictionary *resultObject = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&e];
    if (resultObject != NULL) {
        if ([resultObject objectForKey:@"stamp"] != nil) {
            
            obj = [teacher getInstance];
            NSString *stampSerial = [[resultObject objectForKey:@"stamp"] objectForKey:@"serial"];
            
            if ([obj.stampType isEqualToString:@"Register"] )
            {
                
                if (! [[DBManager getSharedInstance] isStampRegistered:stampSerial])
                {
                    NSMutableArray *students = [[DBManager getSharedInstance] getUnregisteredStudents];
                    student *ss = [students objectAtIndex:obj.registerIndex];
                    NSString *serial = [ss getSerial];
                    
                    if ([serial isEqualToString:@""])
                    {
                        [ss setSerial:stampSerial];
                        [ss setIsactivated:1];
                        [[DBManager getSharedInstance] addStudent:ss];
                        [[DBManager getSharedInstance] removeUnregisteredStudent:ss];
                        obj.unregisteredStudentsCount--;
                        
                        
                        
                    }
                    else { NSLog(@"This student is already registered");}
                    if (obj.registerIndex == obj.unregisteredStudentsCount)
                    {
                        [self registerClicked:@""];
                    }
                    else
                    {
                        [self displayName:obj.registerIndex];
                    }

                }
                else
                {
                    UIAlertView *alertView =[[UIAlertView alloc] initWithTitle:@"Error"
                                                                       message:@"Stamp Registered Already"
                                                                      delegate:self
                                                             cancelButtonTitle:@"ok"
                                                             otherButtonTitles:nil,nil];
                    [alertView show];
                }
                
            }
            else if ([obj.stampType isEqualToString:@"Reward"])
            {
                if ( [[DBManager getSharedInstance] isStampRegistered:stampSerial])
                {
                    student *ss = [[DBManager getSharedInstance] getStudent:stampSerial];
                    [self.studentPoints setEnabled:YES];
                    [self.studentName setEnabled:YES];
                    [[DBManager getSharedInstance] addPoint:ss];
                    NSString *name = [ss getName];
                    NSInteger score = [ss getScore];
                    NSString *scoreDisplay = [NSString stringWithFormat:@"+%d", score];
                    
                    [self.studentName setText:[ss getName]];
                    [self.studentPoints setText:scoreDisplay];
                }
                else
                {
                    UIAlertView *alertView =[[UIAlertView alloc] initWithTitle:@"Error"
                                                                       message:@"Must Register Stamp First"
                                                                      delegate:self
                                                             cancelButtonTitle:@"ok"
                                                             otherButtonTitles:nil,nil];
                    [alertView show];
                }

            }
            
            else if([obj.stampType isEqualToString:@"Items"])
            {
                if ( [[DBManager getSharedInstance] isStampRegistered:stampSerial])
                {
                    student *ss = [[DBManager getSharedInstance] getStudent:stampSerial];
                    item *ii = [[DBManager getSharedInstance] getItem:obj.itemId];
                    
                    NSInteger score = [ss getScore];
                    NSInteger cost = [ii getCost];
                    if (score >= cost)
                    {
                        NSInteger newScore;

                        if((score - cost) >= [ss getPointsearned] )
                        {
                            // Keep pointsearned the same, and update the new score. Then add a transaction and update timestamp.
                            NSInteger newScore = (score - cost);
                            [[DBManager getSharedInstance] sellItemWithitem:ii student:ss score:newScore pointsEarned:[ss getPointsearned] ];
                            NSString *scoreDisplay = [NSString stringWithFormat:@"%d - %d = %d",score, cost, newScore];
                            [self.studentPoints setText:scoreDisplay];


                        }
                        else //if ((score - cost) < [ss getPointsearned])
                        {
                            // Set pointsearned to the score. Update the score and points earned, add a transaction and update timestamp
                            newScore = (score - cost);
                            [[DBManager getSharedInstance] sellItemWithitem:ii student:ss score:newScore pointsEarned:newScore ];
                            NSString *scoreDisplay = [NSString stringWithFormat:@"%d - %d = %d",score, cost, newScore];
                            [self.studentPoints setText:scoreDisplay];


                        }
                    }
                    else
                    {
                        NSString *alertMsg = [NSString stringWithFormat:@"Your Score: %d --- Item Cost: %d",score, cost];
                        UIAlertView *alertView =[[UIAlertView alloc] initWithTitle:@"Not Enough Points"
                                                                           message:alertMsg
                                                                          delegate:self
                                                                 cancelButtonTitle:@"ok"
                                                                 otherButtonTitles:nil,nil];
                        [alertView show];
                    }
                    
                }
                else
                {
                    UIAlertView *alertView =[[UIAlertView alloc] initWithTitle:@"Error"
                                                                       message:@"Must Register Stamp First"
                                                                      delegate:self
                                                             cancelButtonTitle:@"ok"
                                                             otherButtonTitles:nil,nil];
                    [alertView show];
                }
            }
            
        }
        else
        {
            NSLog(@"Stamp Didn't Return Valid Serial (>.<)");
        }
    }
}



- (IBAction)registerClicked:(id)sender {
    [self.pointCategory setTitle:@"" forState:UIControlStateNormal];
    [self.pointCategory setEnabled:NO];
    [self.studentPoints setText:@""];
    [self.studentPoints setEnabled:NO];
    [self.studentPoints setText:@""];
    [self.settings setEnabled:NO];
    [self.settings setTitle:@"" forState:UIControlStateNormal];

    obj = [teacher getInstance];
    obj.registerIndex=0;
    NSMutableArray *unregisteredStudents = [[DBManager getSharedInstance]getUnregisteredStudents];
    NSInteger countUnreg = [unregisteredStudents count];
    obj.unregisteredStudentsCount = countUnreg;
    if (countUnreg != 0)
    {
        obj.stampType = @"Register";
        [self displayName:obj.registerIndex];
        [self.nextStudent setTitle:@"Next Student" forState:UIControlStateNormal];
        [self.nextStudent setEnabled:YES];
    }
    else
    {
        UIAlertView *alertView =[[UIAlertView alloc] initWithTitle:@"Congratulations!"
                                                           message:@"All Students Registered"
                                                          delegate:self
                                                 cancelButtonTitle:@"ok"
                                                 otherButtonTitles:nil,nil];
        [alertView show];
        [self.studentName setText:@""];
        [self.studentName setEnabled:NO];
        [self.nextStudent setTitle:@"" forState:UIControlStateNormal];
        [self.nextStudent setEnabled:NO];
        obj.stampType = @"Reward";
        [self.settings setEnabled:YES];
        [self.settings setTitle:@"logout" forState:UIControlStateNormal];

        
    }
}

- (IBAction)nextStudentClicked:(id)sender
{
    NSInteger keyCount = obj.unregisteredStudentsCount;
    
    if (obj.registerIndex == (keyCount-1) || keyCount == 0)
    {
        [self registerClicked:@""];
    }
    else
    {
        obj.registerIndex++;
        [self displayName:obj.registerIndex];
    }
}

- (void) displayName:(NSInteger)index
{
    obj = [teacher getInstance];
    // First unregistered student
    NSMutableArray *unregisteredStudents = [[DBManager getSharedInstance]getUnregisteredStudents];
    student *ss = [unregisteredStudents objectAtIndex:index];
    NSString *name = [ss getName];
    [self.studentName setText:name];
}


- (IBAction)categoriesClicked:(id)sender {
    
    // UI SETUP
    
    obj = [teacher getInstance];
    [self.studentName setText:@""];
    [self.pointCategory setEnabled:YES];
    [self.nextStudent setTitle:@"" forState:UIControlStateNormal];
    [self.nextStudent setEnabled:NO];
    [self.studentPoints setEnabled:NO];
    [self.studentPoints setText:@""];
    [self.settings setEnabled:YES];
    [self.settings setTitle:@"logout" forState:UIControlStateNormal];
    obj.stampType = @"Reward";
    
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Categories:"
                                                      message:@""
                                                     delegate:self
                                            cancelButtonTitle:@"Back"
                                            otherButtonTitles:nil];
    [message setTag:1];
    NSInteger index = 0;
    NSMutableArray *categories = [[DBManager getSharedInstance]getCategories];
    NSInteger keyCount = [categories count];
    obj.categoriesCount = keyCount;
    while (index != keyCount)
    {
        
        category *cc = [categories objectAtIndex:index++];
        NSString *name = [cc getName];
        [message addButtonWithTitle:name];
    }
    
    // Add a New Category Button
    
    //[message addButtonWithTitle:@"+"];
    
    [message show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == [alertView cancelButtonIndex]) {
        [self.pointCategory setEnabled:NO];
    }
    // If a category is clicked
    if (alertView.tag == 1) {
        obj = [teacher getInstance];
        NSMutableArray *categories = [[DBManager getSharedInstance]getCategories];

        // If the back button wasn't clicked
        if (buttonIndex-1 < obj.categoriesCount && buttonIndex != 0)
        {
            category *cc = [categories objectAtIndex:buttonIndex-1];
            NSString *categoryName = [cc getName];
            obj.categoryId = [cc getCID];
            if([categoryName isEqualToString:@"+"])
            {
                NSLog(@"Clicked +");
                // Create an alertView for adding a new Category
                UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Add New Category:"
                                                                  message:@"Enter Category Name and Description"
                                                                 delegate:self
                                                        cancelButtonTitle:@"Create Category"
                                                        otherButtonTitles:nil];
                [message setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
                UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
                [message addGestureRecognizer:singleTap];
                UITextField *categoryName = [message textFieldAtIndex:0];
                categoryName.placeholder = @"Category Name";
                
                UITextField *category = [message textFieldAtIndex:1];
                category.placeholder = @"Category Description";
                [[message textFieldAtIndex:1] setSecureTextEntry:NO];
                
                [message setTag:2];
                [message show];
            }
            else
            {
                obj.categoryId = [cc getCID];
                [self.pointCategory setTitle:categoryName forState:UIControlStateNormal];
            }
        }
        else
        {
            obj.categoryId = 0;
            [self.pointCategory setTitle:@"" forState:UIControlStateNormal];
        }
    }
    
    // If add a new Category button is clicked
    if (alertView.tag ==2) {
        // Add add a new category
        NSString *categoryName = [alertView textFieldAtIndex:0].text;
        NSString *categoryDescr = [alertView textFieldAtIndex:1].text;
        
        // If Item Name or Cost is Missing
        if ([categoryName isEqualToString:@""] || [categoryDescr isEqualToString:@""])
        {
            UIAlertView *alertView =[[UIAlertView alloc] initWithTitle:@"Category Error"
                                                               message:@"Name and Description Required"
                                                              delegate:self
                                                     cancelButtonTitle:@"ok"
                                                     otherButtonTitles:nil,nil];
            [alertView setTag:3];
            [alertView show];
        }
        
        else
        {
            // Add the category to the database
            [self categoriesClicked:alertView];
        }
        
        
    }
    if (alertView.tag ==3) {
        // Go to Categories View because Create Item Failed
        [self categoriesClicked:alertView];
    }
    
    // If Items are clicked
    if (alertView.tag ==4) {
        obj = [teacher getInstance];
        NSMutableArray *items = [[DBManager getSharedInstance]getItems];
        
        // If the button is not Back
        if (buttonIndex-1 < obj.itemCount && buttonIndex != 0)
        {
            item *ii = [items objectAtIndex:buttonIndex-1];
            NSString *itemName = [ii getName];
            
            if([itemName isEqualToString:@"+"])
            {
                NSLog(@"Clicked +");
                // Create an alertView for adding a new Category
                UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Add New Item:"
                                                                  message:@"Enter Item Name and Cost"
                                                                 delegate:self
                                                        cancelButtonTitle:@"Create Item"
                                                        otherButtonTitles:nil];
                [message setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
                UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
                [message addGestureRecognizer:singleTap];
                UITextField *itemName = [message textFieldAtIndex:0];
                itemName.placeholder = @"Item Name";
                
                UITextField *item = [message textFieldAtIndex:1];
                item.placeholder = @"Category Description";
                [[message textFieldAtIndex:1] setSecureTextEntry:NO];
                
                [message setTag:5];
                [message show];
            }
            else
            {
                obj.itemId = [ii getIID];
                NSString *cost = [NSString stringWithFormat:@"%d", [ii getCost]];
                [self.studentName setText:itemName];
                NSString *costDisplay = [NSString stringWithFormat:@"-%@",cost];
                [self.studentPoints setText:costDisplay];
            }
        }
        // If back was clicked
        else
        {
            obj.itemId = 0;
            obj.stampType = @"Reward";
            [self.pointCategory setTitle:@"" forState:UIControlStateNormal];
        }

    }
    if (alertView.tag == 6) {
        if (buttonIndex > 0) {
            [[DBManager getSharedInstance] pushChanges];
            [self performSegueWithIdentifier:@"logout" sender:self];
        }
    }
}

- (IBAction)itemsClicked:(id)sender
{
    obj = [teacher getInstance];
    [self.studentName setText:@""];
    [self.studentName setEnabled:YES];
    [self.pointCategory setEnabled:NO];
    [self.pointCategory setTitle:@"" forState:UIControlStateNormal];
    [self.nextStudent setTitle:@"" forState:UIControlStateNormal];
    [self.nextStudent setEnabled:NO];
    [self.studentPoints setEnabled:YES];
    [self.studentPoints setText:@""];
    [self.settings setEnabled:YES];
    [self.settings setTitle:@"logout" forState:UIControlStateNormal];
    obj.stampType = @"Items";
    
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Items:"
                                                      message:@""
                                                     delegate:self
                                            cancelButtonTitle:@"Back"
                                            otherButtonTitles:nil];
    [message setTag:4];
    NSInteger index = 0;
    NSMutableArray *items = [[DBManager getSharedInstance]getItems];
    NSInteger keyCount = [items count];
    obj.itemCount = keyCount;
    while (index != keyCount)
    {
        
        item *ii = [items objectAtIndex:index++];
        NSString *name = [ii getName];
        [message addButtonWithTitle:name];
    }
    [message show];
}

- (IBAction)categoryClicked:(id)sender {
    [self categoriesClicked:sender];
}

@end
