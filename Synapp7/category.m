//
//  category.m
//  Synapp7
//
//  Created by Josh on 7/7/14.
//  Copyright (c) 2014 Josh Nussbaum. All rights reserved.
//

#import "category.h"

@implementation category


// Constructors

-(id) init
{
    self = [super init];
    if (self)
    {
        self->cid = 0;
        self->name = @"";
    }
    return self;
}

-(id) initWithcid:(NSInteger)cid_ name:(NSString *)name_
{
    self = [super init];
    if (self)
    {
        self->cid = cid_;
        self->name = name_;
    }
    return self;
}

// Creation Functions

-(void) setCID:(NSInteger)cid_
{
    self->cid = cid_;
}

-(void) setName:(NSString *)name_
{
    self->name = name_;
}

// Read Functions

-(NSInteger) getCID
{
    return self->cid;
}

-(NSString *) getName
{
    return self->name;
}

@end
