//
//  ViewController.h
//  Synapp7
//
//  Created by Josh on 6/30/14.
//  Copyright (c) 2014 Josh Nussbaum. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "teacher.h"
#import "student.h"
#import "item.h"
#import "DBManager.h"

@interface ViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate>
{
    teacher *obj;
}

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

- (IBAction)loginValidate:(id)sender;
- (IBAction)backgroundTap:(id)sender;

- (IBAction)exit:(id)sender;
@end
