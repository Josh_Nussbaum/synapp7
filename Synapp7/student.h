//
//  student.h
//  Synapp7
//
//  Created by Josh on 7/3/14.
//  Copyright (c) 2014 Josh Nussbaum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface student : NSObject
{
    NSInteger uid;
    NSString *name;
    NSInteger lvl;
    NSInteger score;
    NSInteger progress;
    NSInteger lvlupamount;
    NSInteger isactivated;
    NSString *serial;
    NSString *timestamp;
    NSInteger pointsearned;
    NSInteger pointsspent;
}

// Constructor

-(id) init;

-(id) initWithuid:(NSInteger)uid_ name:(NSString *)name_ lvl:(NSInteger)lvl_ lvlupamount:(NSInteger)lvlupamount_ progress:(NSInteger)progress_ serial:(NSString *)serial_ score:(NSInteger)score_ pointsearned:(NSInteger)pointsearned_ pointsspent:(NSInteger)pointsspent_ isactivated:(NSInteger)isactivated_ timestamp:(NSString *)timestamp_;


// Creation Functions
- (void)setUID:(NSInteger )uid_;
- (void)setName:(NSString *)name_;
- (void)setLvl:(NSInteger )lvl_;
- (void)setScore:(NSInteger )score_;
- (void)setProgress:(NSInteger )progress_;
- (void)setLvlupamount:(NSInteger )lvlupamount_;
- (void)setIsactivated:(NSInteger )isactivated_;
- (void)setSerial:(NSString *)serial_;
- (void)setTimestamp:(NSString *)timestamp_;
- (void)setPointsearned:(NSInteger)pointsearned_;
- (void)setPointsspent:(NSInteger)pointsspent_;

// Read Functions

- (NSInteger )getUID;
- (NSString *)getName;
- (NSInteger )getLvl;
- (NSInteger )getScore;
- (NSInteger )getProgress;
- (NSInteger )getLvlupamount;
- (NSInteger )getIsactivated;
- (NSString *)getSerial;
- (NSString *)getTimestamp;
- (NSInteger )getPointsearned;
- (NSInteger )getPointsspent;

// Update Functions
-(void) addPoint;

// Misc Functions

// IF When you buy an Item your total points is less than your earned points. Earned points - Total points = New Earned points
-(void) buyItem:(NSInteger )cost;

@end
