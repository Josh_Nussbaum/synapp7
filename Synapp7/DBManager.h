//
//  DBManager.h
//  Synapp7
//
//  Created by Josh on 7/2/14.
//  Copyright (c) 2014 Josh Nussbaum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "student.h"
#import "category.h"
#import "teacher.h"
#import "item.h"

@interface DBManager : NSObject
{
    NSString *databasePath;
    NSString *SynappDB;
    teacher *obj;
}

+(DBManager*)getSharedInstance;
-(BOOL)createDB;

// Creation Functions
-(void) setStudents:(NSMutableArray *)students;
-(void) setUnregisteredStudents:(NSMutableArray *)unregisteredStudents;
-(void) setItems:(NSMutableArray *)items;
-(void) setCategories:(NSMutableArray *)categories;

// Read Functions
-(NSMutableArray *) getStudents;
-(student *)        getStudent:(NSString *)serial;
-(NSMutableArray *) getUnregisteredStudents;
-(NSMutableArray *) getItems;
-(item *)           getItem:(NSInteger )iid;
-(NSMutableArray *) getCategories;

// Update Functions
-(void) addStudent:(student *)ss;
-(void) removeUnregisteredStudent:(student *)ss;
-(void) registerStudent:(NSInteger)uid;

// Misc Functions
-(BOOL) isStampRegistered:(NSString*)serial;
-(void) addPoint:(student *)ss;
-(void) sellItemWithitem:(item * )ii student:(student *)ss score:(NSInteger )score_ pointsEarned:(NSInteger )pointsEarned_;
-(void) pushChanges;
-(void) updateTables;

@end
